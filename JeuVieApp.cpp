#include <iostream>
#include <string>

#include "Grid.h"
class grid;

int main(int argc, char* argv[])
{
    grid* grid = new ::grid(10,10);
    grid->init_random();
    grid->display();
    std::string input = "";
    while (input != "0")
    {
        grid->iterate();
        grid->display();
        std::cin >> input;
    }
    return 0;
}
