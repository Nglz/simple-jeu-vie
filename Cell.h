#pragma once
#include <vector>

class cell
{
private:
    bool is_alive_;
    int x_;
    int y_;
    bool next_state_;

    std::vector<cell*> neighbors_;

public:
    cell(const int x, const int y, const bool state): is_alive_(state), x_(x), y_(y), next_state_(state)
    {
    }

    int get_x() const { return x_; }
    int get_y() const { return y_; }

    bool is_alive() const { return is_alive_; }
    void set_neighbors(const std::vector<cell*>& neighbors);
    void clear_neighbors();
    void update_next_state();
    void apply_next_state() { is_alive_ = next_state_; }

private:
    int alive_neighbors();
};
