#include "Cell.h"

void cell::set_neighbors(const std::vector<cell*>& neighbors)
{
    neighbors_ = neighbors;
}

void cell::clear_neighbors()
{
    neighbors_.clear();
}

void cell::update_next_state()
{
    const int aliveNeighbors = alive_neighbors();
    switch(aliveNeighbors)
    {
    case 2: // stay in same state
        next_state_ = is_alive();
        break;
    case 3: // alive next iteration
        next_state_ = true;
        break;
    default:
        next_state_ = false;
    }
}

int cell::alive_neighbors()
{
    int res = 0;
    for (auto it = neighbors_.begin(); it != neighbors_.end(); ++it)
    {
        const auto cell = *it;
        if (cell->is_alive()) ++res;
    }
    return res;
}
