#include "Grid.h"

#include <iostream>

void grid::display()
{
    for (auto it = cells_.begin(); it != cells_.end(); ++it)
    {
        const cell* cell = *it;
        if (cell->is_alive())
            std::cout << "O ";
        else std::cout << "X ";
        if (cell->get_x() == width_-1)
            std::cout << std::endl;
    }
}

void grid::init_random()
{
    for (int i = 0; i<height_; ++i)
    {
        for (int j = 0; j< width_; ++j)
            cells_.push_back(new cell(j, i,  rand()%2 == 0));
    }
    set_neighbors();
}

void grid::iterate()
{
    // next state
    for (auto it = cells_.begin(); it != cells_.end(); ++it)
    {
        const auto cell = *it;
        cell->apply_next_state();
    }
    // prepapre next iteration
    for (auto it = cells_.begin(); it != cells_.end(); ++it)
    {
        const auto cell = *it;
        cell->update_next_state();
    }
}

void grid::set_neighbors()
{
    for (auto it = cells_.begin(); it != cells_.end(); ++it)
    {
        const auto ret = new std::vector<cell*>;
        const auto cell = *it;
        const int x = cell->get_x();
        const int y = cell->get_y();
        std::vector<int> xs = {x-1, x, x+1};
        std::vector<int> ys = {y-1, y, y+1};

        for (auto nx = xs.begin(); nx != xs.end(); ++nx)
        {
            for (auto yx = ys.begin(); yx != ys.end(); ++yx)
            {
                auto neighbor = find_cell(*nx, *yx);
                if (neighbor == nullptr)
                    continue;
                ret->push_back(neighbor);
            }
        }
        cell->set_neighbors(*ret);
        cell->update_next_state();
    }
}

cell* grid::find_cell(int x, int y)
{
    if (x < 0) x = width_-1;
    if (x >= width_) x = 0;
    
    if (y < 0) y = width_-1;
    if (y >= width_) y = 0;
    
    
    const int index = y*width_+x;
    if (index < cells_.size() && index >= 0)
        return cells_[index];
    return nullptr;
}
