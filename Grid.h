#pragma once
#include <vector>

#include "Cell.h"

class grid
{
    int height_;
    int width_;
    std::vector<cell*> cells_;

public:
    grid(const int h, const int w): height_(h), width_(w)
    {
    }

    void display();
    void init_random();
    void iterate();

private:
    void set_neighbors();
    cell* find_cell(int x, int y);
};
